i=0;
j=0;
while (true) {
    j++;
    console.log(`Valor de j=${j}`);
    if (j==3) {
        break;
    }
    i++;
    console.log(`valor de i=${i}`);
    if (i==2) {
        break;
    }
    console.log('Fim do laço interno.');
    // reiniciar i para entrar novamente no laço interno
    i=0;
}
console.log('Fim do laço externo.');

