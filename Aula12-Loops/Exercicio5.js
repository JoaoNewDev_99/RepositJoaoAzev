let n = 0;

console.log('Contando até 1000...')
for (let n = 0; n < 1000; n++) {
    if (n === 500) {
        console.log('Cheguei na metade...');
    }
    if (n > 990 && n < 1000) {
        console.log('Chegando no final...');
    }
}
console.log('Terminei!');

