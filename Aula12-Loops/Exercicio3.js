const rl = require('readline-sync');

const n = 0;
while (n === 0) {
        const n = Number(rl.question('Digite um numero entre 1 e 10: '));
        if (n > 10) {
            console.log('Numero inválido.');
            break;
        } else {
            console.log('Obrigado.');
            break;
        }
}