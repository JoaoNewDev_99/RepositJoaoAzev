const rl = require('readline-sync');

const triangulo = 0;
const n = 0;

while (triangulo === 0) {
    const n = Number(rl.question('Digite um numero de 1 a 10: '));
    if (n > 0 && n <= 10) {
        if (n === 1) {
            console.log('*');
            break;
        }
        if (n === 2) {
            console.log('*');
            console.log('**');
            break;
        }
        if (n === 3) {
            console.log('*');
            console.log('**');
            console.log('***');
            break;
        }
        if (n === 4) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            break;
        }
        if (n === 5) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            console.log('*****');
            break;
        }
        if (n === 6) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            console.log('*****');
            console.log('******');
            break;
        }
        if (n === 7) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            console.log('*****');
            console.log('******');
            console.log('*******');
            break;
        }
        if (n === 8) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            console.log('*****');
            console.log('******');
            console.log('*******');
            console.log('********');
            break;
        }
        if (n === 9) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            console.log('*****');
            console.log('******');
            console.log('*******');
            console.log('********');
            console.log('*********');
            break;
        }
        if (n === 10) {
            console.log('*');
            console.log('**');
            console.log('***');
            console.log('****');
            console.log('*****');
            console.log('******');
            console.log('*******');
            console.log('********');
            console.log('*********');
            console.log('**********');
            break;
        }
    } else {
        console.log('Numero invalido. Fim.');
        break;
    }
}