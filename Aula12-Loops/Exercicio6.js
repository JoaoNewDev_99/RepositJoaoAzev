const rl = require('readline-sync');

const jogo = 'x';
const segredo = 1200;
const palpite = 0;

while (jogo === 'x') {
    const palpite = Number(rl.question('Digite um numero: '));
    if (palpite > segredo) {
    console.log('Tente um numero menor.');
    }
    if (palpite < segredo) {
        console.log('Tente um numero maior.');
    }
    if (palpite === segredo) {
        console.log('Parabéns!');
        break;
    }
}