const rl = require('readline-sync');

const num1 = Number(rl.question('Numero 1: '));
const num2 = Number(rl.question('Numero 2: '));

if (num1 === num2) {
	console.log('Yeah! Os dois sao iguais.');
} else {
	console.log('Numeros sao diferentes.');
}

console.log('Fim.');
