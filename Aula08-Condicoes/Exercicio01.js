const rl = require('readline-sync');

const nome = rl.question('Seu nome: ');
const idade = Number(rl.question('Idade: '));

if (idade >= 18) {
	console.log(`Pode passar!`);
} else {
	console.log(`Volte outro dia, ${nome}.`);
}
