console.log(

`Operações de adição:
${2 + 2}
${4 + 1.5}
${'ab' + 'cd'}
${'ab' + 2}

Operações de subtração:
${2 - 2}
${4 - 1.5}
${'ab' - 'cd'}
${'ab' - 2}`

)
