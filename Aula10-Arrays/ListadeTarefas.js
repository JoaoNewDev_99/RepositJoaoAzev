const rl = require('readline-sync');
const fs = require('fs');

const tarefas = [];

let continuar = 's';

while (continuar == 's') {
        // Pedimos a tarefa
        const tarefa = rl.question('Digite uma tarefa: ');
        // Adicionamos ela ao array de tarefas
        tarefas.push(tarefa);

        continuar = rl.question('Continuar (s/n)? ');
}

// Ao final do programa, iteramos sobre o array...

for (i=0; i<tarefas.length; i++) {
    const tarefa = tarefas[i];
    fs.appendFileSync('./tarefas.txt', `${tarefa}\n`);
}

console.log('Fim do programa');

