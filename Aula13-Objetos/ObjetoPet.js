const donoDePet = {
    nome: 'João',
    pet: {
        nome: 'Bud',
        raca: 'Vira-lata',
        idade: 3
    }
}
console.log(
    `
    O nome do dono do pet é ${donoDePet.nome}
    E ele é dono do pet ${donoDePet.pet.nome}.
    Seu pet tem ${donoDePet.pet.idade} anos
    e é da raça ${donoDePet.pet.raca}.
    `
)