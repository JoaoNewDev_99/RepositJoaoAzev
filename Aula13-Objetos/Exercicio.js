const pessoa = {
    nome: 'João',
    idade: 24,
    generoMusicaPreferido: 'Rock',
}
console.log(
    `
    O nome da pessoa é ${pessoa.nome},
    ele tem ${pessoa.idade} anos
    e gosta muito de ${pessoa.generoMusicaPreferido}.
    `
)