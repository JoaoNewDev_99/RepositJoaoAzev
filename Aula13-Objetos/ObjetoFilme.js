barbie = {
    titulo: 'Barbie',
    direcao: 'Greta Gerwig',
    ano: 2023,
    elenco: [
        {nome: 'Margot Robbie', papel: 'Barbie'},
        {nome: 'Ryan Gosling', papel: 'Ken'},
        {nome: 'Issa Rae', papel: 'Barbie Prefeita'}
        //'Margot Robbie: Barbie','Ryan Gosling: Ken','Issa Rae: Barbie Prefeita'
    ],
    jahVi: false
}
/*
ator1 = barbie.elenco[0]
ator2 = barbie.elenco[1]
ator3 = barbie.elenco[2]

console.log(
    `
    Filme: ${barbie.titulo}
    Ano: ${barbie.ano}
    Direção: ${barbie.direcao}
    ${barbie.jahVi ? 'Sim, já vi' : 'Não vi'}
    `
)
for (let index = 0; index < barbie.elenco.length; index++) {
    const element = barbie.elenco[index];
    console.log(barbie.elenco[index]);
}
for (let index = 0; index < barbie.elenco.length; index++) {
    barbie.elenco[0] = 'Xuxa';
    console.log(barbie.elenco[index]);
}*/
console.log(
    `
    Nome: ${barbie.elenco[0].nome}, Papel: ${barbie.elenco[0].papel}
    Nome: ${barbie.elenco[1].nome}, Papel: ${barbie.elenco[1].papel}
    Nome: ${barbie.elenco[2].nome}, Papel: ${barbie.elenco[2].papel}
    `
)
// Alterou o 'Margot Robbie' para 'Xuxa'
barbie.elenco[0].nome = 'Xuxa';
console.log('Elenco: ');
for (let i = 0; i < barbie.elenco.length; i++) {
    console.log(
        `${barbie.elenco[i].nome}(${barbie.elenco[i].papel})`
    )
    
}

