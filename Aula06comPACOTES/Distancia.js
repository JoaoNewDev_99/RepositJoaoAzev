const rl = require('readline-sync')

let v = rl.question('Velocidade: ')
let t = rl.question('Tempo: ')
let d = Number(v) * Number(t)

console.log()
console.log(`Temos uma equação onde a velocidade é igual a ${v}, e o tempo é igual a ${t}.`)
console.log(`A resposta desta equação é ${d}.`)
