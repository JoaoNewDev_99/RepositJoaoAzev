const rl = require('readline-sync');

const nome = rl.question('Nome: ');

// se *não* for informado um nome, imprime ´nome é obrigatório´
!nome && console.log('nome é obrigatório');

// se *for* informado um nome, imprime ´Oi + nome + !!`
!nome || console.log(`Oi, ${nome}!!`);

