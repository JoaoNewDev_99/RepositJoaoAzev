const rl = require('readline-sync');

const ano = rl.question('Ano: ');
const nome = rl.question('Nome: ');
const idade =  rl.question('Idade: ');
const email = rl.question('E-mail: ');

/*
    question() retorna uma String.

    Por isso precisamos converter ano e idade para um valor numérico, para
    podermos fazer operações matemáticas com eles.
    Para isso usamos Number()
*/

const anoDeNascimento = Number(ano) - Number(idade)

console.log(`Ano de nascimento: ${anoDeNascimento}`)
