const rl = require('readline-sync')

const nota1 = Number(rl.question('Nota 1: '))
const nota2 = Number(rl.question('Nota 2: '))
const nota3 = Number(rl.question('Nota 3: '))

const media = (nota1 + nota2 + nota3) / 3

console.log(`Média: ${media}`)
