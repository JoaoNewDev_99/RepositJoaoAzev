let frutas = ["Banana", "Morango", "Abacaxi", "Laranja", "Ameixa"];

console.log('O índice do Abacaxi será listado abaixo:');
console.log(frutas.indexOf('Abacaxi'));

console.log();
console.log('O tamanho do array é:')
console.log(frutas.length);

// Uma forma de usar FOR
/*
for (let fruta of frutas) {
    console.log(fruta)
}
*/