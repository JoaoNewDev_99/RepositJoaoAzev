// Esta é a variável 
let dizOi;

/* Aqui estamos associando a variável
 dizOi a uma função.
 Entre as chaves escrevemos o código
 da função.
*/
dizOi = function(nome) {
    console.log(`Oi, ${nome}!`);
    console.log("Como vai você?");
}

dizOi('João');
