const rl = require('readline-sync');

const comidasPreferidas = ['Arroz', 'Feijão', 'Aimpim', 'Ovo', 'Alface'];

console.log(comidasPreferidas);

console.log(
    `
    Essas são as minhas comidas preferidas:
    `
    
    /*` Uma forma de listar array com o conteúdo um embaixo do outro:
    Essas são as minhas comidas preferidas:

    ${comidasPreferidas[0]}
    ${comidasPreferidas[1]}
    ${comidasPreferidas[2]}
    ${comidasPreferidas[3]}
    ${comidasPreferidas[4]}
    `*/
);
// Listando o array em loop
for (let index = 0; index < comidasPreferidas.length; index++) {
    console.log(comidasPreferidas[index]);
}
console.log();

const preferidaUsuario = rl.question('Qual a sua comida preferida? ');
comidasPreferidas[1] = preferidaUsuario;

for (let index = 0; index < comidasPreferidas.length; index++) {
    console.log(comidasPreferidas[index]);    
}
