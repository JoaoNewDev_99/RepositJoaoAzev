const rl = require('readline-sync')

const tarefas = [ 
    {tarefa: 'Fazer aplicação.', prioridade: 'B'},
    {tarefa: 'Dar banho no dog.', prioridade: 'A'},
    {tarefa: 'Estudar para a prova.', prioridade: 'C'}
]

process.on('exit', function (code) {
    console.log('Fim da aplicação.');
});

function cadastrar() {
    console.log("Nova tarefa:")
    const valTarefa = rl.question(">tarefa: ")
    const valPrioridade = rl.question(">Prioridade (A, B, C): ")

    tarefas.push(
        {
            tarefa: valTarefa,
            prioridade: valPrioridade
        }
    )
    console.log('Tarefa Cadastrada')
}

function listar() {
    console.table(tarefas)
}

function excluir() {
    console.table(tarefas)
    const indice = rl.question('Indice(index) da tarefa a ser excluida: ')
    const tarefaExcluida = tarefas.splice(indice, 1)
    console.log(`Excluida: ${tarefaExcluida}`)
}

function modificar() {
    console.table(tarefas)
    const indice = rl.question('Qual tarefa?: ')
    const tarefa = rl.question('Modificacao: ')
    tarefas[indice] = tarefa
    console.log("Tarefa modificada")
}


while(true) {
console.log(
`
--------------------------
Controle de Tarefas
--------------------------
(C)adastrar tarefa
(L)istar tarefas
(E)xcluir tarefa concluida
(M)odificar tarefa
(S)air
`
)

const comando = rl.question('? ').toUpperCase();

switch (comando) {
    case "S":
        process.exit(0);
    break;
    //
    case "C":
        let novaTarefa = 'S'
        while (novaTarefa =='S') {
            cadastrar()
            novaTarefa = rl.question("Deseja cadastrar uma nova tarefa? (s/n): ").toUpperCase();
        }
    break;
    //
    case "L":
        listar();
    break;
    //
    case "E":
        let excluirTarefa = 'S'
        while (excluirTarefa =='S') {
            excluir()
            excluirTarefa = rl.question("Deseja excluir outra tarefa? (s/n): ").toUpperCase();
        }
    break;
    //
    case "M":
        modificar();
    break;
    //
    default:
        console.log('Opção incorreta.')
    break;
}

/*if (comando == 'S') {
    break;
} else if (comando == 'C') {
    let continuar = 'S'
    while (continuar =='S') {
        cadastrar()
        continuar = rl.question("Deseja cadastrar uma nova tarefa? (s/n): ").toUpperCase();
    }
    
} else if (comando == 'L') {
    listar()

} else if (comando == 'E') {
    let continuar = 'S'
    while (continuar =='S') {
        excluir()
        continuar = rl.question("Deseja excluir outra tarefa? (s/n): ").toUpperCase();
    }

} else if (comando == 'M') {
    modificar()

}*/

rl.question('Tecle ENTER para continuar.')
}