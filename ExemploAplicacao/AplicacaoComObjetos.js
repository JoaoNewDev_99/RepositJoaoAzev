const rl = require('readline-sync')

const tarefas = []

process.on('exit', function (code) {
    console.log('fim')
});


function cadastrar() {
    console.log("Nova tarefa:")
    const valTarefa = rl.question(">tarefa: ")
    const valPrioridade = rl.question(">Prioridade (A, B, C): ")

    tarefas.push(
        {
            tarefa: valTarefa,
            prioridade: valPrioridade
        }
    )
    console.log('Tarefa Cadastrada')
}

function listar() {
    console.table(tarefas)
}

function excluir() {
    console.table(tarefas)
    const indice = rl.question('Indice da tarefa: ')
    const tarefaExcluida = tarefas.splice(indice, 1)
    console.log(`Exclui: ${tarefaExcluida[0].tarefa}`)
}

function modificar() {
    console.table(tarefas)
    const indice = rl.question('Qual tarefa?: ')
    const campo = rl.question('Modificar qual campo? ')
    const modificacao = rl.question("Novo valor: ")
    tarefas[indice][campo] = modificacao
    
    console.log("Tarefa modificada")
}


while(true) {
console.log(
`
Controle de Tarefas
----
(C)adastrar tarefa
(L)istar tarefas
(E)xcluir tarefa concluida
(M)odificar tarefa
(S)air
`
)

const comando = rl.question('? ').toUpperCase()


switch (comando) {
    case "S":
        process.exit(0);
    case "C":
        cadastrar()
        break;
    case "L":
        listar()
        break;
    case "M":
        modificar()
        break;
    case "E":
        excluir()
        break;
    default:
        console.log("Opcao invalida")
        break;
}

rl.question('Tecle ENTER para continuar')
}