const nome = 'João'
const idade = 24
const email = 'grz@oi.xyz'

const ano = 2023
const anoDeNascimento = ano - idade

//console.log(`Ano de nascimento: ${ano - idade}`)
//console.log(`Ano de nascimento: ${anoDeNascimento}`)

console.log(nome, idade, email, anoDeNascimento)
console.log()

let v = 900
let t = 11
let d = v * t

console.log(`Temos uma equação onde a velocidade é igual a ${v}, e o tempo é igual a ${t}.`)
console.log(`A resposta desta equação é ${d}.`)

v = 500
t = 8
d = v * t

console.log()
console.log(`Temos uma segunda equação onde a velocidade é igual a ${v}, e o tempo é igual a ${t}.`)
console.log(`A resposta desta equação acima é ${d}`)
